<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="{{ route('dashboard') }}" class="d-inline-block white">
            <img src="/images/logo_iddrives2.ico" alt="">
        </a>
    </div>
    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar-mobile">
        <span class="navbar-text ml-md-3 mr-md-auto">
            {{-- <span class="badge bg-success"> Online </span> --}}
        </span>
        <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="/images/user.png" alt="" class="rounded-circle" width="35">
                    <span> {{ Auth::user()->name }} </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right font">
                    <a href="" class="dropdown-item"><i class="icon-profile"></i> โปรไฟล์
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item font"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="icon-switch2"></i> ออกจากระบบ </a>
                    <form action="{{ route('logout') }}" id="logout-form" method="POST">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>
