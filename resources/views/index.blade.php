<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Material+Icons" rel="stylesheet">
    <title>Test</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->

    <!-- Bootstrap+Vue -->
</head>
<body>
    <div id="app">
        <app-component></app-component>
    </div>
</body>
</html>
