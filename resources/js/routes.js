import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)
import Home from './pages/Home'
import AllServices from './pages/AllServices'
import AllNews from './pages/Allnews'
import AllBusinessOp from './pages/AllBusinessOp'
import Contact from './pages/Contact'
import Location from './pages/Location'
import AboutCompany from './pages/AboutCompany'

const routes = [
    { 
        path: '/',          
        component: Home     , 
        name: 'index'    
    },
    {
        path: '/allservices',
        name: 'allservices',
        component: AllServices
    },
    {
        path: '/allnews',
        name: 'allnews',
        component: AllNews
    },
    {
        path: '/allbusinessop',
        name: 'allbusinessop',
        component: AllBusinessOp
    },
    {
        path: '/contact',
        name: 'contact',
        component: Contact
    },
    {
        path: '/location',
        name: 'location',
        component: Location
    },
    {
        path: '/aboutcompany',
        name: 'aboutcompany',
        component: AboutCompany
    }
    ];

const Router = new VueRouter({
    routes,
    mode: 'history',
    // linkExactActiveClass: 'is-active',
});

export default Router
