require('./bootstrap');

import Vue from 'vue';
import Vuetify from 'vuetify';
import router from './routes.js';
import store from '@store';



Vue.use(Vuetify);


Vue.component('app-component', require('./App.vue').default);

const app = new Vue({
    el: '#app',
    router,
    store,
    vuetify: new Vuetify({
        icons: 
        { iconfont: 'md' } 
    
    }),
});

