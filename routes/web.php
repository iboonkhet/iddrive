<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






// Route::namespace('Auth')->prefix('backend')->group(function () {
//     Route::get('login', [\App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
//     Route::post('login', [\App\Http\Controllers\Auth\LoginController::class, 'login']);
//     Route::post('logout', 'LoginController@logout')->name('logout');
// });

Route::middleware(['auth:sanctum', 'verified'])->prefix('backend')->group(function () {
    Route::get('dashboard', [\App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
});





Route::view('/','welcome');
Route::view('/{any}','index');
Route::view('/{any}/{any1}', 'index');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
